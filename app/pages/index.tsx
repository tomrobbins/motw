import Head from "next/head";
import Layout from "../components/layout";
import styles from "../styles/Home.module.css";
import { getUserCount } from "./api/users";

export async function getServerSideProps() {
  return {
    props: {
      userCount: await getUserCount(),
    }, // will be passed to the page component as props
  };
}

export default function Home(props: { userCount: number }) {
  return (
    <Layout>
      <Head>
        <title>MOTW Online</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>Welcome to MOTW online!</h1>

        <h2>There are {props.userCount} users in the db</h2>
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by <img src="/vercel.svg" alt="Vercel Logo" className={styles.logo} />
        </a>
      </footer>
    </Layout>
  );
}
