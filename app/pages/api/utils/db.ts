import pg, { Pool } from "pg";
import { DATABASE_URL } from "./settings";

// parse bigint as Number. this will break for 64bit integers
pg.defaults.parseInt8 = true;

const pool = new Pool({
  connectionString: DATABASE_URL,
  max: 5,
  ssl: {
    rejectUnauthorized: false,
  },
});

async function query(text: string, params: any[] = []) {
  return pool.query(text, params);
}

export default { query };
