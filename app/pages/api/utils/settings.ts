function getEnvVar(name: string, defaultValue: string) {
  if (process.env[name]) {
    return process.env[name];
  } else {
    return defaultValue;
  }
}

// export const POSTGRES_DB = getEnvVar("POSTGRES_DB", "postgres");
// export const POSTGRES_USER = getEnvVar("POSTGRES_USER", "postgres");
// export const POSTGRES_PASSWORD = getEnvVar("POSTGRES_PASSWORD", "postgres");
// export const POSTGRES_HOSTNAME = getEnvVar("POSTGRES_HOSTNAME", "localhost");
// export const POSTGRES_PORT = 5432;

export const DATABASE_URL = getEnvVar(
  "DATABASE_URL",
  "postgresql://postgres:postgres@localhost:5432/postgres",
);

export const SECRET = process.env.SECRET;

// export const AUTH_DATABASE_URL = `postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOSTNAME}:${POSTGRES_PORT}/${POSTGRES_DB}?synchronize=true`;
export const AUTH_DATABASE_URL = `${DATABASE_URL}?synchronize=true&ssl=no-verify`;

export const GOOGLE_ID = process.env.GOOGLE_ID;
export const GOOGLE_SECRET = process.env.GOOGLE_SECRET;
