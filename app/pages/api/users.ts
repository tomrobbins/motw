import { NextApiRequest, NextApiResponse } from "next";
import db from "./utils/db";

export async function getUserCount() {
  const { rows } = await db.query("select count(*) from app.users;");
  if (rows.length !== 1) {
    throw new Error("could not find any users");
  }
  return rows[0].count;
}

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  res.status(200).json({ userCount: await getUserCount() });
}
