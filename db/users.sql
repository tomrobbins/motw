create table app.users (
    id serial primary key not null,
    type text not null default 'normal',
    first_name text not null,
    last_name text not null,
    created_at timestamptz default current_timestamp,
    updated_at timestamptz default current_timestamp,
    constraint valid_type check (type in ('normal', 'admin'))
);

create trigger users_modified_at
    before update on app.users for each row
    execute procedure set_modified_timestamp ();

