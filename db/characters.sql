create table app.characters (
    id serial primary key not null,
    user_id integer references app.users,
    name text not null,
    created_at timestamptz default current_timestamp,
    updated_at timestamptz default current_timestamp
);

create trigger characters_modified_at
    before update on app.characters for each row
    execute procedure set_modified_timestamp ();

