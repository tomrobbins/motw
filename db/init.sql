\ir functions.sql
create schema app;

\ir users.sql
\ir characters.sql
select
    append_search_path (current_user, 'app');

